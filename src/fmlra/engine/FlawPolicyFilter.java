package fmlra.engine;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fmlra.engine.AstResultSet.FlawEntryClass;

public class FlawPolicyFilter {
	
    private DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    private DocumentBuilder dBuilder;
    private Document doc;
    private XPath xPath = XPathFactory.newInstance().newXPath();
    private boolean DenyByDefault;
    
	
	public void LoadPolicyFile(String filePath) {
		File inputFile = new File(filePath);
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(1);
		}
		try {
			doc = dBuilder.parse(inputFile);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		doc.getDocumentElement().normalize();
		
		NodeList nlPolicy = null;
		try {
			nlPolicy = (NodeList) xPath.compile("/policies").evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DenyByDefault = true;
		if (nlPolicy.item(0).getAttributes().getNamedItem("Default") != null) {
			if(nlPolicy.item(0).getAttributes().getNamedItem("Default").getNodeValue().toLowerCase().equals("allow")) {
				DenyByDefault = false;	
			}
		}
	}
	
	private static boolean match(String text, String pattern)
	{
		return text.matches(pattern.replace("?", ".?").replace("*", ".*?"));
	}
	
	public void FlawFilter(FlawEntryClass flaw) {
		
		if (doc == null) {
			flaw.category = 1;
			return;
		}
		
		NodeList nlPolicy = null;
		try {
			nlPolicy = (NodeList) xPath.compile("/policies/policy").evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        for (int i = 0; i < nlPolicy.getLength(); i++) {
        	
        	boolean flagNotMatch = false;
        	NamedNodeMap node = nlPolicy.item(i).getAttributes();
        	
        	if (node.getNamedItem("CweID") != null) {
        		if ( !match(Integer.toString(flaw.cweID), node.getNamedItem("CweID").getNodeValue())) {
        			flagNotMatch = true;
        			continue;
        		}
        	}
        	if (node.getNamedItem("FlawID") != null) {
        		if ( !match(flaw.flawID, node.getNamedItem("FlawID").getNodeValue())) {
        			flagNotMatch = true;
        			continue;
        		}
        	}
        	if (node.getNamedItem("Module") != null) {
        		if ( !match(flaw.module, node.getNamedItem("Module").getNodeValue())) {
        			flagNotMatch = true;
        			continue;
        		}
        	}
        	if (node.getNamedItem("SourceFile") != null) {
        		if ( !match(flaw.sourceFilePath, node.getNamedItem("SourceFile").getNodeValue())) {
        			flagNotMatch = true;
        			continue;
        		}
        	}
        	if (node.getNamedItem("Url") != null) {
        		if ( !match(flaw.url, node.getNamedItem("Url").getNodeValue())) {
        			flagNotMatch = true;
        			continue;
        		}
        	}
        	
        	if (flagNotMatch == false && node.getNamedItem("Action") != null) {
        		switch (node.getNamedItem("Action").getNodeValue().toLowerCase()) {
        		case "deny":
					flaw.category = 1;
					return;
        		case "allow":
					flaw.category = 0;
					return;
				}
        	}
        }
        
        // Aplica o default
		flaw.category = DenyByDefault ? 1 : 0;
		return;
	}
}














