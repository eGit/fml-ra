package fmlra.engine;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option.Builder;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.text.DecimalFormat;

import jfml.FuzzyInferenceSystem;
import jfml.JFML;
import jfml.knowledgebase.KnowledgeBaseType;
import jfml.knowledgebase.variable.AggregatedFuzzyVariableType;
import jfml.knowledgebase.variable.FuzzyVariableType;
import jfml.knowledgebase.variable.KnowledgeBaseVariable;
import jfml.knowledgebase.variable.TsukamotoVariableType;
import jfml.rule.AntecedentType;
import jfml.rule.ClauseType;
import jfml.rule.ConsequentType;
import jfml.rule.FuzzyRuleType;
import jfml.rulebase.AnYaRuleBaseType;
import jfml.rulebase.MamdaniRuleBaseType;
import jfml.rulebase.TskRuleBaseType;
import jfml.rulebase.TsukamotoRuleBaseType;
import jfml.term.FuzzyTermType;
import jfml.term.TsukamotoTermType;
import jfml.test.CreateJapaneseDietAssessmentMamdaniExampleXML;
import fmlra.connector.csv.ConnectorCSV;
import fmlra.connector.testcase.TestCase;
import fmlra.connector.veracode.Veracode;
import fmlra.engine.AstResultSet.AstType;

@SuppressWarnings("unused")
public class Main {
	
	static String FmlFilePath;
	static String PolicyFilePath;
	static String ResultsFilePath;
	static boolean isVerbose;
	enum enumConnectorType {veracode, testcase, csv};
	static enumConnectorType ConnectorType;
	
	public static void print(String txt, boolean verboseOnly) {
		if ((verboseOnly && isVerbose == true) || verboseOnly == false) {
			System.out.println(txt);
		}
	}
	
	public static void main(String[] args) {
		
		
		CommandLine commandLine;
        Option cliParam_Connector = Option.builder("c")
            .required(false).hasArg().argName("connectorType")
            .desc("Connector a ser utilizado: TestCase, Veracode, CSV")
            .longOpt("Connector")
            .build();
        Option cliParam_CreateFml = Option.builder("i")
            .required(false).hasArg().argName("file")
            .desc("Cria o XML do FML-RA que contem as regras e base de conhecimento FML")
            .longOpt("initxml")
            .build();
        Option cliParam_FmlFilePath = Option.builder("x")
            .required(false).hasArg().argName("file")
            .desc("Local do XML que contem as regras e base de conhecimento FML")
            .longOpt("fml")
            .build();
        Option cliParam_ResultsToEval = Option.builder("r")
            .required(false).hasArg().argName("file")
            .desc("Resultados da ferramenta AST a ser avaliado (para connectors Veracode & CSV)")
            .longOpt("Results")
            .build();
        Option cliParam_Policies = Option.builder("p")
            .required(false).hasArg().argName("file")
            .desc("Local do arquivo que contem as pol�ticas do aplicativo")
            .longOpt("Policies")
            .build();
        Option cliParam_Help = Option.builder("h")
            .required(false)
            .desc("Exibe este help")
            .longOpt("help")
            .build();
        Option cliParam_Verbose = Option.builder("v")
            .required(false)
            .desc("Exibe o output em modo verbose")
            .longOpt("verbose")
            .build();
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();
		
        String[] testArgs = args;
        
        options.addOption(cliParam_Connector);
        options.addOption(cliParam_CreateFml);
        options.addOption(cliParam_FmlFilePath);
        options.addOption(cliParam_Policies);
        options.addOption(cliParam_ResultsToEval);
        options.addOption(cliParam_Help);
        options.addOption(cliParam_Verbose);
        
        try
        {
            commandLine = parser.parse(options, testArgs);
            
            if (commandLine.hasOption("help") || commandLine.getOptions().length == 0)
            {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( System.lineSeparator()
                		+ "fmlra -c TYPE -x FML_FILE [-p POLICY_FILE] [-v]"
                		+ System.lineSeparator()
                		+ 			 "fmlra -i FML_FILE", options );
                System.exit(1);
            }
            
            // Valida combina��es dos par�metros
            if (commandLine.hasOption("Policies") || commandLine.hasOption("Connector") || commandLine.hasOption("fml")) {
            	if (!commandLine.hasOption("Connector") || !commandLine.hasOption("fml")) {
            		System.out.println("Conjunto de par�metros inv�lidos.");
	                System.out.println("Veja o help com o par�metro '--help' ou '-h'.");
	                System.exit(1);
            	}
            }
            if (commandLine.hasOption("initxml")) {
            	if (commandLine.getOptions().length > 1) {
            		System.out.println("Conjunto de par�metros inv�lidos.");
	                System.out.println("Veja o help com o par�metro '--help' ou '-h'.");
	                System.exit(1);
            	}
            }
            
            // Tratamento dos params
            
            if (commandLine.hasOption("verbose"))
            {
            	isVerbose = true;
            } else {
            	isVerbose = false;
            }
            
            if (commandLine.hasOption("initxml"))
            {
                System.out.print("Regra FML ser�o criadas em: ");
                System.out.println(commandLine.getOptionValue("initxml"));
                // Atualiza o XML do FMLRA
        		createXmlFile_FIS_fmlra(commandLine.getOptionValue("initxml"));
        		System.exit(0);
            }
            
            if (commandLine.hasOption("Connector"))
            {
            	switch (commandLine.getOptionValue("Connector").toLowerCase()) {
				case "veracode":
					ConnectorType = enumConnectorType.veracode;
					if (!commandLine.hasOption("Results")) {
						System.out.println("Informe o par�metro de resultados (-r).");
						System.exit(1);
					}
					ResultsFilePath = commandLine.getOptionValue("Results");
					break;
				case "csv":
					ConnectorType = enumConnectorType.csv;
					if (!commandLine.hasOption("Results")) {
						System.out.println("Informe o par�metro de resultados (-r).");
						System.exit(1);
					}
					ResultsFilePath = commandLine.getOptionValue("Results");
					break;
				case "testcase":
					ConnectorType = enumConnectorType.testcase;
					if (commandLine.hasOption("Results")) {
						System.out.println("O par�metro de resultados (-r) n�o � suportado para o TestCase.");
						System.exit(1);
					}
					break;
				default:
					System.out.println("Conector n�o suportado!");
					System.exit(1);
					break;
				}
                System.out.print("Conector: ");
                System.out.println(commandLine.getOptionValue("Connector"));
            }

            if (commandLine.hasOption("fml"))
            {
                System.out.print("Arquivo FML: ");
                FmlFilePath = commandLine.getOptionValue("fml");
                System.out.println(FmlFilePath);
            }

            if (commandLine.hasOption("Policies"))
            {
                System.out.print("Arquivo de pol�ticas: ");
                PolicyFilePath = commandLine.getOptionValue("Policies");
                System.out.println(PolicyFilePath);
            }

            {
            	if (commandLine.getArgs().length > 0) {
	                String[] remainder = commandLine.getArgs();
	                System.out.print("Par�metro n�o reconhecido: ");
	                for (String argument : remainder)
	                {
	                    System.out.print(argument);
	                    System.out.print(" ");
	                }
	                System.out.println("");
	                System.out.println("Veja o help com o par�metro '--help' ou '-h'.");
	                System.exit(1);
            	}
            }
        }
        catch (ParseException exception)
        {
            System.out.print("Erro na avalia��o dos par�metros: ");
            System.out.println(exception.getMessage());
            System.out.println("Veja o help com o par�metro '--help' ou '-h'.");
            System.exit(1);
        }
		
		// Carrega o XML do FMLRA
		File xml = new File(FmlFilePath);
		FuzzyInferenceSystem fs = JFML.load(xml);
		
		// Declare inputs values
		KnowledgeBaseVariable score = fs.getVariable("Score");
		KnowledgeBaseVariable category = fs.getVariable("Category");
		KnowledgeBaseVariable exploit = fs.getVariable("Exploit");
		
		
		// Utiliza o connector para veracode
		AstResultSet RetSet = new AstResultSet();
		
		try {
			
			
			switch (ConnectorType) {
			case veracode:
				Veracode veracode = new Veracode();
				RetSet = veracode.LoadReultSet(ResultsFilePath);
				break;
			case csv:
				ConnectorCSV csv = new ConnectorCSV();
				RetSet = csv.LoadReultSet(ResultsFilePath);
				break;
			case testcase:
				RetSet = TestCase.LoadReultSet("");
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		// Flaw Policy Filter Init
		FlawPolicyFilter policyFilter = new FlawPolicyFilter();
		int flagReject = 0;
		int flagWarn = 0;
		if (PolicyFilePath != null) {
			policyFilter.LoadPolicyFile(PolicyFilePath);
		}

		// Printing the FuzzySystem
		if (isVerbose) {
			System.out.println(fs.toString());
		}
		System.out.println("----------------------------");
		// Realiza avalia��o
		for (int i = 0; i < RetSet.flawEntry.size(); i++) {
			
			// Avalia categoriza��o
			policyFilter.FlawFilter(RetSet.flawEntry.get(i));
			
			// Set inputs values
			score.setValue(RetSet.flawEntry.get(i).severity);
			category.setValue(RetSet.flawEntry.get(i).category);
			exploit.setValue(RetSet.flawEntry.get(i).exploitLevel);
			
			// Fuzzy inference
			fs.evaluate();
			
			// Get output
			KnowledgeBaseVariable result =  fs.getVariable("Result");
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			df.setMinimumFractionDigits(0);

			if (result.getValue() >= 1 && result.getValue() < 2)
				flagWarn++;
			if (result.getValue() >= 2)
				flagReject++;
			
			System.out.println(
					"[" + df.format(result.getValue()) + "]"
        			+ " " + RetSet.flawEntry.get(i).astType
        			+ " | exp: " + RetSet.flawEntry.get(i).exploitLevel
        			+ " | sev: " + RetSet.flawEntry.get(i).severity
        			+ " | cat: " + RetSet.flawEntry.get(i).category
        			+ " | cweID: " + RetSet.flawEntry.get(i).cweID
        			+ " | flawID: " + RetSet.flawEntry.get(i).flawID
        			+ " | module: " + RetSet.flawEntry.get(i).module
        			+ (RetSet.flawEntry.get(i).astType == AstType.SAST ? 
        					" | Line: " + RetSet.flawEntry.get(i).sourceLine
        					+ " | File: " + RetSet.flawEntry.get(i).sourceFilePath : "")
        			+ (RetSet.flawEntry.get(i).astType == AstType.DAST ? 
        					" | url: " + RetSet.flawEntry.get(i).url : "")
        	);
		}
		
		System.out.println("----------------------------");
		System.out.println("Total: " + RetSet.flawEntry.size());
		System.out.println("Accept: " + (RetSet.flawEntry.size() - flagReject - flagWarn));
		System.out.println("Warn: " + flagWarn);
		System.out.println("Reject: " + flagReject);
		
		if (flagReject > 0) {
			System.exit(2);
		} else if (flagWarn > 0) {
			System.exit(1);
		} else {
			System.exit(0);
		}
		
	}
	
	
	public static void fmlra_addRule(MamdaniRuleBaseType rbMam
			, FuzzyTermType scoreVal, FuzzyTermType categoryVal, FuzzyTermType exploitVal
			, FuzzyTermType resultVal, String RuleName, float weight
			, FuzzyVariableType score, FuzzyVariableType category, FuzzyVariableType exploit, FuzzyVariableType result
			) {
		
		FuzzyRuleType rule = new FuzzyRuleType(RuleName, "and", "MIN", weight);

		AntecedentType ant1 = new AntecedentType();
		if (score != null && scoreVal != null)
			ant1.addClause(new ClauseType(score, scoreVal));
		if (category != null && categoryVal != null)
			ant1.addClause(new ClauseType(category, categoryVal));
		if (exploit != null && exploitVal != null)
			ant1.addClause(new ClauseType(exploit, exploitVal));
		rule.setAntecedent(ant1);

		ConsequentType con1 = new ConsequentType();
		con1.addThenClause(result, resultVal);
		rule.setConsequent(con1);
		
		rbMam.addRule(rule);
		
	}
	

	public static boolean createXmlFile_FIS_fmlra(String filePath) {
		
		FuzzyInferenceSystem fuzzysys = new FuzzyInferenceSystem("fmlra");
		KnowledgeBaseType kb = new KnowledgeBaseType();
		fuzzysys.setKnowledgeBase(kb);
		
		// * Input vars
		
		// Score (Input)
		FuzzyVariableType score = new FuzzyVariableType("Score", 0, 10);
		kb.addVariable(score);
		
		FuzzyTermType scoreNone = new FuzzyTermType("None",
				FuzzyTermType.TYPE_singletonShape,(new float[] {0f}));
		score.addFuzzyTerm(scoreNone);
		
		FuzzyTermType scoreLow = new FuzzyTermType("Low",
				FuzzyTermType.TYPE_rectangularShape,(new float[] {0.0001f, 3.9999f}));
		score.addFuzzyTerm(scoreLow);
		
		FuzzyTermType scoreMedium = new FuzzyTermType("Medium",
				FuzzyTermType.TYPE_rectangularShape,(new float[] {4f, 6.9999f}));
		score.addFuzzyTerm(scoreMedium);

		FuzzyTermType scoreHigh = new FuzzyTermType("High",
				FuzzyTermType.TYPE_rectangularShape,(new float[] {7f, 8.9999f}));
		score.addFuzzyTerm(scoreHigh);

		FuzzyTermType scoreCritical = new FuzzyTermType("Critical",
				FuzzyTermType.TYPE_rectangularShape,(new float[] {9f, 10f}));
		score.addFuzzyTerm(scoreCritical);
		
		
		// Category (Input)
		FuzzyVariableType category = new FuzzyVariableType("Category", 0, 1);
		kb.addVariable(category);

		FuzzyTermType categoryAllowed = new FuzzyTermType("Allowed",
				FuzzyTermType.TYPE_singletonShape,(new float[] {0f}));
		category.addFuzzyTerm(categoryAllowed);

		FuzzyTermType categoryProhibited = new FuzzyTermType("Prohibited",
				FuzzyTermType.TYPE_singletonShape,(new float[] {1f}));
		category.addFuzzyTerm(categoryProhibited);
		
		
		// Exploit (Input)
		FuzzyVariableType exploit = new FuzzyVariableType("Exploit", 0, 2);
		kb.addVariable(exploit);

		FuzzyTermType exploitUnlikely = new FuzzyTermType("Unlikely",
				FuzzyTermType.TYPE_singletonShape,(new float[] {0f}));
		exploit.addFuzzyTerm(exploitUnlikely);

		FuzzyTermType exploitNeutral = new FuzzyTermType("Neutral",
				FuzzyTermType.TYPE_singletonShape,(new float[] {1f}));
		exploit.addFuzzyTerm(exploitNeutral);

		FuzzyTermType exploitLikely = new FuzzyTermType("Likely",
				FuzzyTermType.TYPE_singletonShape,(new float[] {2f}));
		exploit.addFuzzyTerm(exploitLikely);
		
		
		
		// * Output vars
		
		// Result (Output)
		FuzzyVariableType result = new FuzzyVariableType("Result", 0, 2);
		result.setType("output");
		result.setDefuzzifierName("COG");
		kb.addVariable(result);

		FuzzyTermType resultAccept = new FuzzyTermType("Accept",
				FuzzyTermType.TYPE_singletonShape,(new float[] {0f}));
		result.addFuzzyTerm(resultAccept);

		FuzzyTermType resultWarning = new FuzzyTermType("Warning",
				FuzzyTermType.TYPE_singletonShape,(new float[] {1f}));
		result.addFuzzyTerm(resultWarning);

		FuzzyTermType resultReject = new FuzzyTermType("Reject",
				FuzzyTermType.TYPE_singletonShape,(new float[] {2f}));
		result.addFuzzyTerm(resultReject);

		
		// * Rule base
		
		MamdaniRuleBaseType rbMam = new	MamdaniRuleBaseType("MamdaniRB-1");
		rbMam.setRuleBaseSystemType(MamdaniRuleBaseType.TYPE_MAMDANI);
		rbMam.setActivationMethod("DRP");
		fuzzysys.addRuleBase(rbMam);
		
		
					// Score, Cat, Exploit, Result
		fmlra_addRule(rbMam, scoreNone, categoryAllowed, exploitUnlikely, resultAccept, "Rule-01", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreNone, categoryAllowed, exploitNeutral, resultAccept, "Rule-02", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreNone, categoryAllowed, exploitLikely, resultAccept, "Rule-03", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreNone, categoryProhibited, exploitUnlikely, resultAccept, "Rule-04", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreNone, categoryProhibited, exploitNeutral, resultAccept, "Rule-05", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreNone, categoryProhibited, exploitLikely, resultAccept, "Rule-06", 1f, score, category, exploit, result);

		fmlra_addRule(rbMam, scoreLow, categoryAllowed, exploitUnlikely, resultAccept, "Rule-07", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreLow, categoryAllowed, exploitNeutral, resultAccept, "Rule-08", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreLow, categoryAllowed, exploitLikely, resultAccept, "Rule-09", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreLow, categoryProhibited, exploitUnlikely, resultAccept, "Rule-10", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreLow, categoryProhibited, exploitNeutral, resultAccept, "Rule-11", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreLow, categoryProhibited, exploitLikely, resultAccept, "Rule-12", 1f, score, category, exploit, result);

		fmlra_addRule(rbMam, scoreMedium, categoryAllowed, exploitUnlikely, resultAccept, "Rule-13", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreMedium, categoryAllowed, exploitNeutral, resultAccept, "Rule-14", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreMedium, categoryAllowed, exploitLikely, resultAccept, "Rule-15", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreMedium, categoryProhibited, exploitUnlikely, resultReject, "Rule-16", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreMedium, categoryProhibited, exploitNeutral, resultReject, "Rule-17", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreMedium, categoryProhibited, exploitLikely, resultReject, "Rule-18", 1f, score, category, exploit, result);

		fmlra_addRule(rbMam, scoreHigh, categoryAllowed, exploitUnlikely, resultWarning, "Rule-19", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreHigh, categoryAllowed, exploitNeutral, resultWarning, "Rule-20", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreHigh, categoryAllowed, exploitLikely, resultWarning, "Rule-21", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreHigh, categoryProhibited, exploitUnlikely, resultReject, "Rule-22", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreHigh, categoryProhibited, exploitNeutral, resultReject, "Rule-23", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreHigh, categoryProhibited, exploitLikely, resultReject, "Rule-24", 1f, score, category, exploit, result);

		fmlra_addRule(rbMam, scoreCritical, categoryAllowed, exploitUnlikely, resultWarning, "Rule-25", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreCritical, categoryAllowed, exploitNeutral, resultWarning, "Rule-26", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreCritical, categoryAllowed, exploitLikely, resultWarning, "Rule-27", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreCritical, categoryProhibited, exploitUnlikely, resultReject, "Rule-28", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreCritical, categoryProhibited, exploitNeutral, resultReject, "Rule-29", 1f, score, category, exploit, result);
		fmlra_addRule(rbMam, scoreCritical, categoryProhibited, exploitLikely, resultReject, "Rule-30", 1f, score, category, exploit, result);
		
		// Save to file
		File xml = new File(filePath);
	    JFML.writeFSTtoXML(fuzzysys, xml);
		
		return true;
	}


}
