package fmlra.engine;

public class AstResultSet {
	
	public java.util.Vector<FlawEntryClass> flawEntry = new java.util.Vector<>();
	
	public enum AstType{
		SAST, DAST, OTHER
	}
	public static class FlawEntryClass {
		// *AST
		public String flawID;
		public String module;
		public int severity;
		public int exploitLevel;
		public int cweID;
		public AstType astType;
		public int category;// Parametro do FMLRA para indicar que houve libera��o/bloqueio
		// SAST
		public String sourceFilePath;
		public int sourceLine;
		// DAST
		public String url;
	}
}
