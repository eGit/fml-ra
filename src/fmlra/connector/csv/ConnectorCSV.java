package fmlra.connector.csv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import fmlra.engine.AstResultSet;
import fmlra.engine.AstResultSet.FlawEntryClass;

public class ConnectorCSV {
	static AstResultSet RetSet = new AstResultSet();
	
	public enum Headers {Score, Exploit, AstType, FlawID, CweID, Module, SourceFile, SourceLine, Url}
	
	public AstResultSet LoadReultSet(String CsvFilePath) {

		Reader in;
		Iterable<CSVRecord> records = null;
		try {
			in = new FileReader(CsvFilePath);
			//records = CSVFormat.RFC4180.withHeader(Headers.class).parse(in);
			records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
		} catch (FileNotFoundException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		for (CSVRecord record : records) {
			AstResultSet.FlawEntryClass flaw = new FlawEntryClass();
			
	    	flaw.cweID = Integer.parseInt(record.get("CweID"));
	    	flaw.exploitLevel = Integer.parseInt(record.get("Exploit"));
	    	flaw.category = 1;
	    	flaw.flawID = record.get("FlawID");
	    	flaw.module = record.get("Module");
	    	flaw.severity = Integer.parseInt(record.get("Score"));
	    	flaw.sourceFilePath = record.get("SourceFile");
	    	flaw.sourceLine = Integer.parseInt(record.get("SourceLine"));
	    	flaw.url = record.get("Url");
	    	
	    	switch (record.get("AstType").toUpperCase()) {
			case "SAST": flaw.astType = AstResultSet.AstType.SAST; break;
			case "DAST": flaw.astType = AstResultSet.AstType.DAST; break;
			default: flaw.astType = AstResultSet.AstType.OTHER; break;
			}
	    	RetSet.flawEntry.add(flaw);
		}
		return RetSet;
	}
}
