package fmlra.connector.testcase;

import fmlra.engine.AstResultSet;
import fmlra.engine.AstResultSet.FlawEntryClass;

public class TestCase {
	static AstResultSet RetSet = new AstResultSet();
	public static AstResultSet LoadReultSet(String param) {

		ResultFeed(0, 0, 0);
		ResultFeed(0, 0, 1);
		ResultFeed(0, 0, 2);
		ResultFeed(0, 1, 0);
		ResultFeed(0, 1, 1);
		ResultFeed(0, 1, 2);

		ResultFeed(1, 0, 0);
		ResultFeed(1, 0, 1);
		ResultFeed(1, 0, 2);
		ResultFeed(1, 1, 0);
		ResultFeed(1, 1, 1);
		ResultFeed(1, 1, 2);

		ResultFeed(4, 0, 0);
		ResultFeed(4, 0, 1);
		ResultFeed(4, 0, 2);
		ResultFeed(4, 1, 0);
		ResultFeed(4, 1, 1);
		ResultFeed(4, 1, 2);

		ResultFeed(7, 0, 0);
		ResultFeed(7, 0, 1);
		ResultFeed(7, 0, 2);
		ResultFeed(7, 1, 0);
		ResultFeed(7, 1, 1);
		ResultFeed(7, 1, 2);

		ResultFeed(10, 0, 0);
		ResultFeed(10, 0, 1);
		ResultFeed(10, 0, 2);
		ResultFeed(10, 1, 0);
		ResultFeed(10, 1, 1);
		ResultFeed(10, 1, 2);
        
		return RetSet;
		
	}
	
	private static void ResultFeed(int score, int category, int exploit) {
		AstResultSet.FlawEntryClass flaw = new FlawEntryClass();
		
		flaw.astType = AstResultSet.AstType.SAST;
    	flaw.cweID = 0;
    	flaw.exploitLevel = exploit;
    	flaw.category = category;
    	flaw.flawID = "";
    	flaw.module = "";
    	flaw.severity = score;
    	flaw.sourceFilePath = "";
    	flaw.sourceLine = 0;
    	flaw.url = "";
    	
    	RetSet.flawEntry.add(flaw);
	}
}
