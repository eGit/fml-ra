package fmlra.connector.veracode;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import java.io.*;

import fmlra.engine.AstResultSet;
import fmlra.engine.AstResultSet.FlawEntryClass;

public class Veracode {	
	AstResultSet RetSet = new AstResultSet();
	public AstResultSet LoadReultSet(String filePath) throws Exception {
		
		File inputFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        XPath xPath = XPathFactory.newInstance().newXPath();
        doc.getDocumentElement().normalize();
        
        // SAST
        NodeList nlFlaw = (NodeList) xPath.compile("/detailedreport/severity/category/cwe/staticflaws/flaw").evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nlFlaw.getLength(); i++) {
        	
        	AstResultSet.FlawEntryClass flaw = new FlawEntryClass();
        	// Get raw values
        	flaw.astType = AstResultSet.AstType.SAST;
        	flaw.category = 1;
        	flaw.cweID = Integer.parseInt(nlFlaw.item(i).getAttributes().getNamedItem("cweid").getNodeValue());
        	flaw.exploitLevel = Integer.parseInt(nlFlaw.item(i).getAttributes().getNamedItem("exploitLevel").getNodeValue());
        	flaw.flawID = nlFlaw.item(i).getAttributes().getNamedItem("issueid").getNodeValue();
        	flaw.module = nlFlaw.item(i).getAttributes().getNamedItem("module").getNodeValue();
        	flaw.severity = Integer.parseInt(nlFlaw.item(i).getAttributes().getNamedItem("severity").getNodeValue());
        	flaw.sourceFilePath = nlFlaw.item(i).getAttributes().getNamedItem("sourcefilepath").getNodeValue()
        			+ nlFlaw.item(i).getAttributes().getNamedItem("sourcefile").getNodeValue();
        	flaw.sourceLine = Integer.parseInt(nlFlaw.item(i).getAttributes().getNamedItem("line").getNodeValue());
        	
        	// Parse values to FMLRA
        	switch (flaw.exploitLevel) {
			case -2: case -1:
				flaw.exploitLevel = 0;
				break;
			case 0:
				flaw.exploitLevel = 1;
				break;
			case 1:	case 2:
				flaw.exploitLevel = 2;
				break;
			default:
				flaw.exploitLevel = 1;
				break;
			}
        	switch (flaw.severity) {
			case 0:
				flaw.severity = 0;
				break;
			case 1: case 2:
				flaw.severity = 3;
				break;
			case 3:
				flaw.severity = 6;
				break;
			case 4:
				flaw.severity = 8;
				break;
			case 5:
				flaw.severity = 10;
				break;
			}
        	
        	RetSet.flawEntry.add(flaw);
        	/*System.out.println(
        			"[" + flaw.astType + "]"
        			+ " | exploitLevel: " + flaw.exploitLevel
        			+ " | cweID: " + flaw.cweID
        			+ " | flawID: " + flaw.flawID
        			+ " | module: " + flaw.module
        			+ " | severity: " + flaw.severity
        			+ " | sourceFilePath: " + flaw.sourceFilePath
        			+ " | sourceLine: " + flaw.sourceLine
        			);*/
        	
        }
        // DAST
        nlFlaw = (NodeList) xPath.compile("/detailedreport/severity/category/cwe/dynamicflaws/flaw").evaluate(doc, XPathConstants.NODESET);
        
        for (int i = 0; i < nlFlaw.getLength(); i++) {
        	
        	AstResultSet.FlawEntryClass flaw = new FlawEntryClass();
        	// Get raw values
        	flaw.astType = AstResultSet.AstType.DAST;
        	flaw.category = 1;
        	flaw.cweID = Integer.parseInt(nlFlaw.item(i).getAttributes().getNamedItem("cweid").getNodeValue());
        	flaw.flawID = nlFlaw.item(i).getAttributes().getNamedItem("issueid").getNodeValue();
        	flaw.module = nlFlaw.item(i).getAttributes().getNamedItem("module").getNodeValue();
        	flaw.severity = Integer.parseInt(nlFlaw.item(i).getAttributes().getNamedItem("severity").getNodeValue());
        	flaw.url = nlFlaw.item(i).getAttributes().getNamedItem("url").getNodeValue();
        	
        	// Parse values to FMLRA
        	flaw.exploitLevel = 1;
        	
        	switch (flaw.severity) {
			case 0:
				flaw.severity = 0;
				break;
			case 1: case 2:
				flaw.severity = 3;
				break;
			case 3:
				flaw.severity = 6;
				break;
			case 4:
				flaw.severity = 8;
				break;
			case 5:
				flaw.severity = 10;
				break;
    		}
            
            RetSet.flawEntry.add(flaw);
        	
		}
        
		return RetSet;
		
	}
}
